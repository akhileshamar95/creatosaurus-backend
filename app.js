const express = require('express')
const app = express();
const bodyParser = require('body-parser');
require('dotenv').config()
const config = require('config');
const jwt =require('jsonwebtoken')
const mongoose=require('mongoose')
const dbconnection = require('./Dbconnect/dbconnect');
const  helmet = require('helmet');
const morgan = require('morgan');
const cors = require('cors');
const compression = require('compression')


if (!config.get('jwtPrivateKey')) {
    console.error('FATAL ERROR: jwtPrivateKey is not defined.');
    process.exit(1);
  }
/************************************
 * @DESC - MIDDLEWARE INIITILIZATION
 * @PACKAGE - EXPRESS
 ***********************************/

app.use( cors() );


/************************************
 * @DESC    - PARSER JSON BODY
 * @PACKAGE - body-parser
 ***********************************/
app.use( bodyParser.urlencoded({ extended : false }) );
app.use( bodyParser.json() );

const payment = require('./server/routes/payment.table');
const UserRouter = require('./server/routes/users.routes');
const adminRouter = require ('./server/routes/admin.routes');
const notification = require ('./server/routes/notification.router');
const email = require('./server/routes/email');


/************************************
 * @DESC    -  ROUTER
 * @PACKAGE -  EXPRESS
 ***********************************/
// middleware for apiRoutes
app.use(express.json());
app.use('/payment',payment);
app.use('/user',UserRouter);
app.use('/admin',adminRouter);
app.use ('/api',notification);
app.use('/email',email);

// const accessLogStream =fs.createWriteStream(
//     path.join(__dirname,'access.log'),
//     {flags:'a'}
// );

  console.log(process.env.NODE_ENV);

 app.use(helmet());
 app.use(morgan("combined"));
 app.use (compression());

app.use(
//    ( req, res, next) =>{
//     const error = new Error('Not found');
//     error.status=404;
//     next(error); 
// })

// app.use((error, req, res, next) =>{
// res. status(error.status ||500);
// res.json({
//     error:{
//         massage:error.massage
//     }
// });

// });

// // api test
(
    req, res, next) =>{
   res.status(200).json({
       massage:'its working!!!!'
    });
});
// if( process.env.NODE_ENV === 'production' ){
    
//     app.use(express.static('client/Components'));
//     app.use('*', (req, res) => {
//         res.sendFile(path.resolve(__dirname, 'client', 'Components', 'index.js' ));
//     })
// }



const port = process.env.PORT || 3001;
require('dotenv').config();
app.get('/', (req, res) => {
    res.send(process.env.PORT);
    
})

app.listen(port, () => {
    console.log(`Server is running on port ${port}.`)
})
