
const mongoose = require('mongoose');
const config = require('../config/config');
const  {db: { MONGO_DB_PW }} = config;
// const monogodb_url = "mongodb://localhost:27017/creatosauros" ;
const monogodb_url  = "mongodb+srv://akhilesh95:" + MONGO_DB_PW + "@cluster0-xfnwr.mongodb.net/creatosauros?retryWrites=true&w=majority" ;

//Connect to MongoDB.
mongoose.Promise = global.Promise;
console.log("mongodb url: " + monogodb_url);
mongoose.connect(monogodb_url, { useNewUrlParser: true , useCreateIndex: true, useUnifiedTopology: true })
    .then(() => console.log('MongoDB Connected'))
    .catch((err) => {
        console.log(err);
        console.log('%s MongoDB connection error. Please make sure MongoDB is running.');
        process.exit();
    });
    module.export = { mongoose };



